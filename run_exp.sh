#! /bin/bash
configFile=${1:/sazae_evo/repeats_short_run_fixed_diff}
nbRuns=${2:5}

currentdate=`date +%m%d%Y_%H-%M-%S`
targetdir=results_$currentdate
echo $targetdir

mkdir $targetdir

dirhere=$(pwd)/$targetdir
dircontainer=/sazae_evo/results

exec docker run --mount type=tmpfs,tmpfs-size=8589934592,target=$dircontainer --mount type=bind,source=$dirhere,target=$dircontainer sazae_evo $configFile $nbRuns
