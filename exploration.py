#!/usr/bin/env python
# coding: utf-8

import sys
import math
from math import pi, sqrt, log10
import numpy as np
import yaml
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


from pde import PDE, CartesianGrid, MemoryStorage, PDEBase, ScalarField, plot_kymograph, UnitGrid, FieldCollection, PlotTracker

from scipy.optimize import curve_fit
from scipy.signal import find_peaks, peak_widths

import matplotlib.pyplot as plt

from qdpy import algorithms, containers, benchmarks, plots
from qdpy.base import ParallelismManager, registry
from scipy.stats.qmc import LatinHypercube

from functools import partial
import os

#Parameters for evolution, n species system
# for each species u1, u2, ... un, u1u1, u1u2, ..., u2u2, u2u3, ... un-1un-1, un-1un, unun
# only mass action
# diffusion for each species
class Individual_GRN:
    def __init__(self, diffusions, parameters, static_species = False):
        self.n_species = len(diffusions)
        self.diffusions = diffusions
        self.parameters = parameters
        if static_species:
            self.diffusions[0] = 0.0 
    def pde(self):
        res = {}
        for i in range(self.n_species):
            eq = [f"{self.diffusions[i]} * laplace(u{i})"]
            for j in range(self.n_species):
                if self.parameters[i,j] > 0.0:
                    eq.append(f"+ {self.parameters[i,j]} * u{j}")
                elif self.parameters[i,j] < 0.0:
                    eq.append(f"{self.parameters[i,j]} * u{j}")
            index = self.n_species # more convenient and fool proof (I assume) than computing the exact index
            for j in range(self.n_species):
                for k in range(j,self.n_species):
                    if self.parameters[i,index] > 0.0:
                        eq.append(f"+ {self.parameters[i,index]} * u{j}*u{k}")
                    elif self.parameters[i,index] < 0.0:
                        eq.append(f"{self.parameters[i,index]} * u{j}*u{k}")
                    index = index + 1
            res[f"u{i}"] = ''.join(eq)
        return res

# Parameters for evolution, 2 species system
# In order:
# rho: activation rate
# rho0: spontaneous activation from substrate (as rho*rho0 in the Flower et al. paper)
# kappa: Michaelis-like parameter for activation
# mu: degradation rate for the activator
# sigma: spontaneous creation of substrate (outside flux?)
# nu: degradation rate for the subtrate
# Da Ds: diffusion rates
class Individual_AS:
    
    def __init__(self, diffusions, parameters, static_species = False):
        self.n_species = len(diffusions)
        if (len(diffusions) > 2):
            print("WARNING: incorrect species number for "+self.__class__.__name__)
        self.diffusions = diffusions
        self.parameters = parameters
        if static_species:
            self.diffusions[0] = 0.0 

    def pde(self):
        res = {"u0" : f"{self.parameters[0]} * u1 * (u0 * u0/(1+{self.parameters[1]} * u0* u0) + {self.parameters[2]}) - {self.parameters[3]} * u0 + {self.diffusions[0]} * laplace(u0)",
               "u1": f"{self.parameters[4]}-{self.parameters[0]} * u1 * (u0 * u0/(1+{self.parameters[1]} * u0* u0) + {self.parameters[2]})-{self.parameters[5]} * u1 + {self.diffusions[1]} * laplace(u1)"}
        return res

# Param order: c1 c2 c3 c4
class Individual_Non_Dim(Individual_AS):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def pde(self):
        res = {"u0" : f"u1 * ((u0 * u0)/(1+{self.parameters[0]} * u0 * u0) + {self.parameters[1]}) - {self.parameters[2]} * u0 + {self.diffusions[0]} * laplace(u0)",
               "u1": f"1- u1 * ((u0 * u0)/(1+{self.parameters[0]} * u0* u0) + {self.parameters[3]}) + {self.diffusions[1]} * laplace(u1)"}
        return res

@registry.register
class LatinHypercubeSampling(algorithms.RandomUniform):
    def __init__(self, container, budget, ind_domain, sample=None, sample_size=50000, **kwargs):
        super().__init__(container, budget, ind_domain=ind_domain, **kwargs)
        if sample is None:
            sampler = LatinHypercube(self.dimension)
            self.sample = sampler.random(n=max(budget,sample_size))
        else:
            self.sample = sample
        self.index = 0

    def _internal_ask(self, base_ind):
        base_ind[:] = self.sample[self.index]
        self.index += 1 # not thread safe?
        return base_ind
    

#Define class parameter with min value, max value, default and scale
class Evo_Parameter:

    def __init__(self, name, min_val, max_val, default=None, scale='linear'):
        self.name = name
        self.min_val = min_val
        self.max_val = max_val
        self.default = default if default is not None else (min_val+max_val)/2.0
        self.scale = scale

    # turn a value from the 0-1 range into a proper value    
    def transform(self,value):
        if self.scale=='log':
            tmp = log10(self.min_val) + value*(log10(self.max_val)-log10(self.min_val))
            return 10**(tmp)
        elif self.scale=='linear':
            return self.min_val + value*(self.max_val-self.min_val)
        else: # should be executable
            return self.scale(self,value)

    def __str__(self):
        return f"PARAM {self.name}: ({self.min_val}, {self.default}, {self.max_val}, {self.scale})"
    
def load_parameters(filename):
    """ Function to automatically load a parameter list from a yaml file"""
    conf = []
    with open(filename,"r") as f:
        loaded = yaml.load(f.read(), Loader=Loader)
        for key in loaded["params"]:
            conf.append(Evo_Parameter(key, float(loaded["params"][key]["min"]), float(loaded["params"][key]["max"]),
                                      default=float(loaded["params"][key]["baseline"]),scale=loaded["params"][key]["scale"]))
    return conf, loaded

def gaussian(x,a, b, mu,sigma):
    return a/(np.sqrt(2*pi*sigma*sigma))*np.exp(-(x-mu)*(x-mu)/(2*sigma*sigma)) + b

def gaussian_simulation(indiv, param_ranges, nspecies = 2, maxTime = 1000,dt=1e-3, a = 1, b = 0.5, normalize=True, feature_ids=[0,3], fitness_scale=20.0, grid_length = 32, diffusion_params = None): 

    gridPDE = CartesianGrid([[0,1]],[grid_length],periodic=[True])
    #u = ScalarField(gridPDE, a, label="Field $u$")
    v = [b / a + 0.001 * ScalarField.random_normal(gridPDE, label="Field $v$"+str(i)) for i in range(nspecies)]
    state = FieldCollection(v)
    #test = Individual(indiv[:nspecies],2*np.reshape(indiv[nspecies:],(nspecies,int(len(indiv)/nspecies - 1)))-1)
    #print("Param range;", param_ranges)
    if param_ranges is not None:
        updated_params = np.array([param_ranges[i].transform(a) for i, a in enumerate(indiv)])
    else : #direct simulation of values
        updated_params = indiv 
    if diffusion_params is None:
        diffusion_params = updated_params[:nspecies]
        updated_params = updated_params[nspecies:]
        
    test = Individual_Non_Dim(diffusion_params,updated_params)
    eq = PDE(test.pde())
    #print(test.pde())
    return gridPDE, eq.solve(state, t_range=maxTime, dt=dt,tracker=['consistency'])

def gaussian_eval(indiv, param_ranges, nspecies = 2, maxTime = 1000,dt=1e-3, a = 1, b = 0.5, normalize=True, feature_ids=[0,3], fitness_scale=20.0, grid_length = 32, allow_reversed_peak = False, **kwargs):
    #gridPDE = UnitGrid([grid_length],periodic=[True])
    ##u = ScalarField(gridPDE, a, label="Field $u$")
    #v = [b / a + 0.001 * ScalarField.random_normal(gridPDE, label="Field $v$"+str(i)) for i in range(nspecies)]
    #state = FieldCollection(v)
    ##test = Individual(indiv[:nspecies],2*np.reshape(indiv[nspecies:],(nspecies,int(len(indiv)/nspecies - 1)))-1)
    #updated_params = np.array([param_ranges[i].transform(a) for i, a in enumerate(indiv)])
    #test = Individual_Non_Dim(grid_length*updated_params[:nspecies],updated_params[nspecies:])
    #eq = PDE(test.pde())
    ydata = []
    fitness = 0
    popt = [-1,-1,-1,-1]
    try:
        # TODO: use **kwargs
        gridPDE, sol = gaussian_simulation(indiv,param_ranges, nspecies = nspecies, maxTime = maxTime,dt=dt, a = a, b = b, normalize=normalize, grid_length = grid_length, **kwargs)#eq.solve(state, t_range=maxTime, dt=dt,tracker=['consistency'])
        xdata = gridPDE.axes_coords[0]
        ydata = sol.data[0]
        ydata = np.concatenate([ydata,ydata]) #so that we can find peaks despite the periodicity
        if normalize:
            ydata = ydata/np.max(ydata)
        peaks, features = find_peaks(ydata,width=2,prominence=1.0)
        #del sol
        #del eq
        #print(features)
        #print(peaks)
        if not math.isnan(ydata[0]) and len(peaks)>0:
            #find the widest peak
            id_peak = np.argmax(np.array(features['widths']))
            #print("id peak:",id_peak)
            relevant_xdata = xdata[:features['right_bases'][id_peak]-features['left_bases'][id_peak]] #we match the length
            relevant_ydata = ydata[features['left_bases'][id_peak]:features['right_bases'][id_peak]]
            #print(relevant_ydata)
            try:
                popt, pcov = curve_fit(gaussian, relevant_xdata, relevant_ydata)
                fitness = np.linalg.norm(relevant_ydata-gaussian(relevant_xdata, *popt))/fitness_scale
            # Change representation
            except Exception as e:
                print(e)
                popt = [-1,-1,-1,-1]
            if allow_reversed_peak:
                try:
                    popt2, pcov2 = curve_fit(gaussian, relevant_xdata, -1*relevant_ydata+np.max(ydata))
                    fitness2 = np.linalg.norm(relevant_ydata+gaussian(relevant_xdata, *popt2)-np.max(ydata))/fitness_scale
                except:
                    fitness2 = 0
                    popt2 = [-1,-1,-1,-1]
                    if popt[0]==-1 or (popt2[0] > -1 and fitness2 < fitness): #we are better
                        return [fitness2],[popt2[i] for i in feature_ids]
    except Exception as inst:
        print("interupted:", sys.exc_info()[0])
        print(inst)
        popt = [-1,-1,-1,-1]
    print(fitness,popt)
    return [fitness],[popt[i] for i in feature_ids]


def plot_indiv(indiv, maxTime = 100, dt = 1e-5, a =1 , b = 3):
    gridPDE = UnitGrid([32])
    #u = ScalarField(gridPDE, a, label="Field $u$")
    #v = b / a + 0.1 * ScalarField.random_normal(gridPDE, label="Field $v$")
    v = [b / a + 0.001 * ScalarField.random_normal(gridPDE, label="Field $v$"+str(i)) for i in range(indiv.n_species)]
    state = FieldCollection(v)
    #test = Individual(indiv[:nspecies],2*np.reshape(indiv[nspecies:],(nspecies,int(len(indiv)/nspecies - 1)))-1)
    eq = PDE(indiv.pde())
    sol = eq.solve(state, t_range=maxTime, dt=dt)
    xdata = gridPDE.axes_coords[0]
    ydata = sol.data[0]
    print("ydata", ydata)
    popt, pcov = curve_fit(gaussian, xdata, ydata,bounds=(0, [100., 100., 100., 100.0]))
    popt2, pcov2 = curve_fit(gaussian, xdata, -1*ydata+ydata.max(),bounds=(0, [100., 100., 100., 100.0]))
    plt.plot(xdata,ydata)
    plt.plot(xdata,gaussian(xdata, *popt))
    plt.plot(xdata,-1*gaussian(xdata, *popt2)+ydata.max())
    plt.legend(["data","fitted gaussian","fitted inverted"])
    
def run(outputdir, params, conf, nspecies=2, algo_type=algorithms.RandomSearchMutPolyBounded):
    if not os.path.isdir(outputdir):
        os.mkdir(outputdir)
        
    grid = containers.Grid(shape=(100,100), max_items_per_bin=1, fitness_domain=((0., 1.),), features_domain=((0., 2.), (0., 2.)))
    algo = algo_type(grid, budget=1000, batch_size=20, dimension=len(params), optimisation_task="minimisation", ind_domain=(0.,1.0))
    logger = algorithms.AlgorithmLogger(algo, log_base_path=outputdir)
    func = partial(gaussian_eval, param_ranges=params, nspecies=nspecies,maxTime=float(conf["maxTime"]),dt=float(conf["dt"]),
                   grid_length=float(conf["grid_length"]),normalize=bool(conf["normalize"]),feature_ids=conf["feature_ids"],
                   fitness_scale=conf["fitness_scale"], diffusion_params=np.array([10**(-2.75),10**(-0.15)]))
    for run in range(int(conf["budget"]/1000)):
        with ParallelismManager("multiprocessing") as pMgr:
            best = algo.optimise(func, executor = pMgr.executor, batch_mode=False)

        path = os.path.join(outputdir, str(run))
        if not os.path.isdir(path):
            os.mkdir(path)
        plots.default_plots_grid(logger, output_dir=path)

    
if __name__ == "__main__":
    
    import logging
    import argparse
    import shutil

    parser = argparse.ArgumentParser()
    parser.add_argument("--outputdir", help="directory to store outputs",type=str,default=".")
    parser.add_argument("--nspecies", help="species number",type=int, default=2)
    parser.add_argument("--conf", help="configuration file",type=str,default="standard_params.conf")
    parser.add_argument("-r","--repeats",help="number of repeats",type=int, default = 1)
    parser.add_argument("-a", "--algo", help="algorithm type. Either a class name from qdpy or LatinHypercubeSampling", type=str, default="RandomSearchMutPolyBounded")
    parser.add_argument("-d", "--debug", help="only try a basic pde simulation",action="store_true")
    args = parser.parse_args()
    if not os.path.isdir(args.outputdir):
        os.mkdir(args.outputdir)
    if os.path.isfile(args.conf):
        shutil.copyfile(args.conf,os.path.join(args.outputdir,os.path.basename(args.conf)))
        params, conf = load_parameters(args.conf)
    else:
        print("ERROR: Configuration file not found")
        exit()

    if args.debug:
        print("test {0}".format(nspecies))
        #using values from the Flower et al. paper
        #indiv = Individual_AS([0.002,0.4],[0.01,0.001,0,0.01,0.015,0])
        indiv = Individual_Non_Dim([p.default for p in params[:nspecies]],[p.default for p in params[nspecies:]])
        print(indiv.pde())
        plot_indiv(indiv)
        exit()
    
    for i in range(args.repeats):
        run(os.path.join(args.outputdir,f"repeat{i}"),params,conf,nspecies=args.nspecies, algo_type=registry[args.algo])
