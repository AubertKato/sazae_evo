#!/bin/bash
set -e

nbRuns=$2
configName=$1
shift; shift;

exec python3 exploration.py --outputdir /sazae/results --conf $configName --repeat $nbRuns
