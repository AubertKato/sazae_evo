import numpy as np
import exploration as pde_run
import pickle
import yaml
from yaml import load, dump
import matplotlib.pyplot as plt
from math import pi
import pandas as pd
import sys

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class Custom_unpickler(pickle._Unpickler):
    #compatibility support for older versions of qdpy
    current_module = {"Individual": "qdpy.phenotype", "Fitness": "qdpy.phenotype"}
    
    def find_class(self, module, name):
        if name in Custom_unpickler.current_module:
            module = Custom_unpickler.current_module[name] #backward compatibility
        sys.audit('pickle.find_class', module, name)
        if self.proto < 3 and self.fix_imports:
            if (module, name) in _compat_pickle.NAME_MAPPING:
                module, name = _compat_pickle.NAME_MAPPING[(module, name)]
            elif module in _compat_pickle.IMPORT_MAPPING:
                module = _compat_pickle.IMPORT_MAPPING[module]
        __import__(module, level=0)
        if self.proto >= 4:
            return pickle._getattribute(sys.modules[module], name)[0]
        else:
            return getattr(sys.modules[module], name)
    
def convert(a, sigma,factor=4.0):
    # Converting Gaussian parameters from exploration to g1, g2 from the mechanical model
    return [a/(factor*np.sqrt(2*np.pi*sigma*sigma)),sigma]

def get_parameter_list(filename):
    with open(filename,"rb") as f:
        paraml = load(f.read(), Loader=Loader)
    return paraml

def get_grid(filename):
    with open(filename,"rb") as f:
        grid = Custom_unpickler(f,fix_imports=True, encoding="ASCII", errors="strict",
          buffers=None).load()["container"]
    return grid

def main(args, pfile):
    distances = []
    params = get_parameter_list(args.params)
    grid = get_grid(pfile)
    confs, setup = pde_run.load_parameters(args.conf)
    if args.direct_values:
        confs = None # We do not use those
    #next, find closest
    all_conditions = {}
    for condition in params:
        #print(condition)
        counter = 0
        all_conditions[condition] = []
        for values in params[condition]:
            if not args.direct_values:
                closest_index = (-1,-1)
                min_dist = 10000000
                for ind in grid.features:
                    if grid.features[ind]:
                        val = np.linalg.norm(convert(*grid.features[ind][0])-np.array(values))
                        if val < min_dist:
                            closest_index = ind
                            min_dist = val
                distances.append(min_dist)
                        
                #print(values, "closest val", np.array(grid.features[closest_index][0]))
                indiv = grid.solutions[closest_index][0]
            else:
                indiv = np.array([float(v) for v in values])
            #print("individual", indiv)
            #print(pde_run.gaussian_eval(indiv,confs,maxTime=float(setup["maxTime"]),
            #        dt=float(setup["dt"]),grid_length=float(setup["grid_length"]),normalize=bool(setup["normalize"]),feature_ids=setup["feature_ids"]))
            if args.save_plot:
                gridPDE, sol = pde_run.gaussian_simulation(indiv,confs,maxTime=float(setup["maxTime"]),dt=float(setup["dt"]),grid_length=float(setup["grid_length"]),normalize=bool(setup["normalize"]),feature_ids=setup["feature_ids"])
                xdata = gridPDE.axes_coords[0]
                ydata = sol.data[0]
                fig = plt.figure()
                ax = fig.add_subplot(111)      
                ax.plot(xdata,ydata)
                fig.savefig(str(condition)+f"_{counter}.png")
                plt.close()
                counter += 1
                #TODO: simulation twice...
                
            if not args.direct_values:
                scaled_values = np.array([confs[i].transform(a) for i, a in enumerate(indiv)])
                #print("parameters:", scaled_values)
            else:
                scaled_values = np.array(indiv)
            scaled_values = np.concatenate((np.array(values), scaled_values, [min_dist]))
            all_conditions[condition].append(scaled_values)
    #print("====\n\n")
    #for condition in all_conditions:
    #    means = np.mean(all_conditions[condition],axis=0)
    #    stds = np.std(all_conditions[condition],axis=0)
        #print(condition+":",', '.join([f"{a:.2E} +/- {b:.2E}" for a,b in zip(means,stds)]))
    return all_conditions, distances

if __name__ == "__main__":
    
    import argparse
    import os
    parser = argparse.ArgumentParser()
    parser.add_argument("--pfile", help="File storing optimization results",type=str,default="final.p")
    parser.add_argument("--repeat_dir", help="Path to the root folder holding repeats. The pfile path will be appended to each directory in it", type=str, default=None)
    parser.add_argument("--params", help="parameters to find",type=str,default="full_conditions_ext3")
    parser.add_argument("--direct_values", help="Use the parameters directly for simulation rather than using the pfile", action="store_true")
    parser.add_argument("--conf", help="configuration file",type=str,default="standard_params.conf")
    parser.add_argument("--save_plot", help="simulate and save the result as plot", action="store_true")
    parser.add_argument("--data_export_file", help="file to save the date",type=str,default="results_ci.dat")
    args = parser.parse_args()
    target_dirs = ["."]
    if args.repeat_dir is not None:
        target_dirs = os.listdir(args.repeat_dir)
    else:
       args.repeat_dir = ""
        
    all_results = {}
    distances = []
    for directory in target_dirs:
        if not os.path.isdir(os.path.join(args.repeat_dir,directory)):
    	    continue
        print(directory)
        results, dists = main(args,os.path.join(args.repeat_dir,directory,args.pfile))
        distances += dists
        for condition in results:
            if condition in all_results:
                all_results[condition] += results[condition]
            else:
                all_results[condition] = results[condition]
    for condition in all_results:
        means = np.mean(all_results[condition],axis=0)
        stds = np.std(all_results[condition],axis=0)
        print(condition+":",', '.join([f"{a:.2E} +/- {b:.2E}" for a,b in zip(means,stds)]))
    print("===")
    print("distances", np.mean(distances),"+/-",np.std(distances))
    confs, setup = pde_run.load_parameters(args.conf)
    data_cols = ["Condition"]+["g1", "g2"]+[f"{confs[i].name}" for i in range(len(confs))]+["min_dist"]
    data_for_export = [[condition]+list(a) for condition in all_results for a in all_results[condition]]
    df = pd.DataFrame(data_for_export,columns=data_cols)
    df.to_csv(args.data_export_file)
BBOB
