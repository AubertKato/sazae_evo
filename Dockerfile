FROM ubuntu:22.04
MAINTAINER naubertkato@is.ocha.ac.jp

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    python3-dev \
    python3-pip \
    python3-yaml

COPY . /sazae
WORKDIR /sazae

RUN pip3 install py-pde qdpy[all]

ENTRYPOINT ["/sazae_evo/entrypoint.sh"]